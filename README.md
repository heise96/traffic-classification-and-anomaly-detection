## **PROJECT DIRECTORY**

<pre>.
+-- biflows_processing.ipynb
+-- sampling.py
+-- biflows_processing.py
+-- preprocessing.sh
+-- csv gen/
|   +-- csv_for_sampling.py
|   +-- csv_merger.py
|   +-- npy_merger.py
|   +-- csv_payload.py
|   +-- npy_header.py
|   +-- csv_statistical_feature.py
|   +-- dataset_barplot.py
+-- src/
|   +-- csv_manager.py
|   +-- log_parser.py
|   +-- log_pcap_merge.py
|   +-- pcap_parser.py
|   +-- util/
|   |   +-- bfl_printer.py
|   |   +-- dir_manager.py
+-- ml_dl_scripts/
|   +-- anomaly_detection.ipynb
|   +-- traffic_classification.ipynb
|   +-- CNN_traffic_classification.ipynb
|   +-- 784_CNN_traffic_classification.ipynb
|   +-- RNN_traffic_classification.ipynb
|   +-- multimodal_traffic_classification.ipynb
+-- rsc/
</pre>

## PREPROCESSING
- ### **biflows_processing.py e preprocessing.sh**
    Con questi file vengono estratti i .dat tramite shelve a partire dal pcap e dal conn.log.labeled della cattura.
    Disponibile in versione script python da lanciare tramite un script bash che ha come parametro la cartella che contiene il file .pcap e il conn.log.labeled . 

    ./preprocessing.sh ~path_to_folder~

- ### **sampling.py**
    Riceve come input un file .dat e un *_sampled.csv, sulle informazioni del secondo campiona il .dat. Automaticamente chiamato da preprocessing.sh.

- ### **csv_for_sampling.py**
    Crea un csv temporaneo usato per campionare il .dat.

- ### **npy/csv_merger.py**
    Unisce i csv/npy delle varie catture. Il prodotto di questo file viene usato come input nel processo di classificazione.

- ### **csv_payload.py**
    Dati i file .dat campionati produce un csv che contiene i primi x byte del payload, label e detailed_label.

- ### **npy_header.py**
    Dati i file .dat campionati produce un data frame dove ogni quintupla/riga contiene una matrice n x m, dove n è il numero di pacchetti per ogni biflusso (primi 20 pacchetti) ed m le feature del header.

- ### **csv_statistical_feature.py**
    Dati i file .dat campionati produce un csv che contiene le feature statistiche, label e detailed_label.

- ### **dataset_barplot.py**
    Produce il barplot delle occorrenze delle detailed_label.

- ### **src/ e src/util**
    Contiene una serie di moduli che vengono importati dai file precedenti.

- ### **rsc/**
    Contiene varie risorse tra cui grafici, csv, file temporanei e dati usati come testing.


## CLASSIFICATION

#### I seguenti script sono stati eseguiti su Google Colab

- ### **anomaly_detection.ipynb**
Contiene il codice che permette di effettuare l'anomaly detection con approcci ML, in particolare con Naïve Bayes, Random Forest, Decision Tree e Bagging.

- ### **traffic_classification.ipynb**
Contiene il codice che permette di effettuare traffic classification con approcci ML, in particolare con Naïve Bayes, Random Forest, Decision Tree e Bagging.

- ### **CNN_traffic_classification.ipynb**
Contiene il codice che permette di effettuare traffic classification con approccio DL tramite una rete Convoluzionale 1D sui primi 576 byte del payload.

- ### **784_CNN_traffic_classification.ipynb**
Contiene il codice che permette di effettuare traffic classification con approccio DL tramite una rete Convoluzionale 1D sui primi 784 byte del payload. Differisce dal precedente per alcuni accorgimenti resi necessari dalle limitazioni presenti nella versione gratuita di Google Colab (RAM limitata).

- ### **RNN_traffic_classification.ipynb**
Contiene il codice che permette di effettuare traffic classification con approccio DL tramite una rete Convoluzionale 2D e LSTM che ha come input 6 feature per i primi 12/20/32/64 pacchetti per ogni biflusso.

- ### **multimodal_traffic_classification.ipynb**
Contiene il codice che permette di effettuare traffic classification con approccio DL tramite una rete multimodale.









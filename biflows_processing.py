import shelve
import traceback
from pprint import pprint
from datetime import datetime
from glob import glob
import sys
import os
from scapy.all import PcapReader, IP, TCP, UDP, DNS, NTP, Raw

#CUSTOM MODULES
sys.path.insert(1, './src/util')
sys.path.insert(1, './src')

import dir_manager as mng
import log_parser as lp
import pcap_parser as pp
import log_pcap_merge as lpm
import re

PATH = ""

def tryint(s):
    try:
        return int(s)
    except:
        return s

def alphanum_key(s):
    """ Turn a string into a list of string and number chunks.
        "z23a" -> ["z", 23, "a"]
    """
    return [ tryint(c) for c in re.split('([0-9]+)', s) ]

def sort_nicely(l):
    """ Sort the given list in the way that humans expect.
    """
    l.sort(key=alphanum_key)

def sort_biflows(all_biflows):
    for quintuple in all_biflows:
        all_biflows[quintuple].sort(key=lambda x : x['ts'])
        all_biflows.sync()

def pck_proc(packet,biflows):
    pp.get_biflows(packet, biflows)

def dict_merge(c, d):
    for k, v in d.items():
        if k in c:
            c[k].extend(v)
        else:
            c[k] = v

def dict_merge_wrapper(biflows_file, dat_files):

    biflows_merged = shelve.open(biflows_file, writeback=True)

    try:
        for i,dat_file in enumerate(dat_files):
            print('--------------------------------')
            print('working on ' + dat_file + ' - ' + str(i))
            start = datetime.now() 

            biflows_to_merge = shelve.open(dat_file)
            dict_merge(biflows_merged, biflows_to_merge)

            biflows_to_merge.close()
            
            #Uncomment for large file
            if i%10==0:
                print('flushing biflows.dat')
                biflows_merged.sync()
            
            end = datetime.now()
            t = end - start
            print(dat_file + ' done in : ' + str(t))
            mng.delete_dir(dat_file,False)
    except:
        print(traceback.format_exc())
        biflows_to_merge.close()

    biflows_merged.close()

def split_log(log_path):
    #SPLITTING LOG FILE
    mng.make_dir(PATH+'splitted_log')
    print('--------------------------------')
    start = datetime.now()
    print('splitting log file: ' + log_path)
    lp.log_split(log_path)
    stop = datetime.now()
    t = stop-start
    print('split done in ' + str(t))

def parse_log():
    mng.make_dir(PATH+'biflows_log')

    #sort log file
    log_path = PATH+'splitted_log/*'
    log_files = glob(log_path)
    sort_nicely(log_files)

    for log_file in log_files:
        print(log_file)
        print('--------------------------------')
        start = datetime.now()
        print('parsing log file: ' + log_file)

        file_log = log_file.split('/')[3]
        print('parsing log file: ' + file_log)
        log_biflows = PATH+'biflows_log/' + file_log + '.dat'

        lp.get_biflows(log_file, log_biflows)

        stop = datetime.now()
        t = stop-start
        print(file_log + ' done in ' + str(t))
        mng.delete_dir(log_file,False)

def get_biflows_log():
    dat_path = PATH+'biflows_log/*'  
    dat_files = glob(dat_path)
    sort_nicely(dat_files)
    dict_merge_wrapper(PATH+'log.dat', dat_files)

    mng.delete_dir(PATH+"splitted_log",True)
    mng.delete_dir(PATH+"biflows_log",True)

def sort_log_dat():
    try:
        print('--------------------------------')
        start = datetime.now()
        print('sorting log.dat')

        biflows = shelve.open(PATH+'log.dat', writeback=True)
        sort_biflows(biflows)
        biflows.close()

        stop = datetime.now()
        t = stop-start
        print('sorting done in ' + str(t))
    except:
        print(traceback.format_exc())
        biflows.close()

def get_biflows_pcap():
    pcap_path = PATH+"pcap/*"
    os.rename(PATH+"pcap/capture",PATH+"pcap/capture0")
    pcap_files = glob(pcap_path)
    sort_nicely(pcap_files)

    mng.make_dir(PATH+"biflows_pcap")

    try:
        for i,pcap_file in enumerate(pcap_files):
            print('--------------------------------')
            print('working on ' + pcap_file[3:] + ' - ' + str(i))
            start = datetime.now()

            pcap_file_name = pcap_file.split('/')[3]
            biflows = shelve.open(PATH+'biflows_pcap/'+ pcap_file_name +'.dat', writeback=True)
            pcap_reader = PcapReader(pcap_file)

            for i, packet in enumerate(pcap_reader):
                pck_proc(packet, biflows)
            biflows.close()

            end = datetime.now()
            t = end - start
            print(pcap_file_name + ' done in : ' + str(t))
            mng.delete_dir(pcap_file,False)
        mng.delete_dir(PATH+"pcap", True)
    except:
        print(traceback.format_exc())
        biflows.close()

def merge_pcap_dat():
    dat_path = PATH+"biflows_pcap/*"
    dat_files = glob(dat_path)
    sort_nicely(dat_files)

    dict_merge_wrapper(PATH+"pcap.dat", dat_files)
    mng.delete_dir(PATH+"biflows_pcap",True)

def sort_pcap_dat():
    print('--------------------------------')
    start = datetime.now()
    print('sorting pcap.dat')

    biflows = shelve.open(PATH+'pcap.dat', writeback=True)
    sort_biflows(biflows)
    biflows.close()

    stop = datetime.now()
    t = stop-start
    print("sorting done in: " + str(t))

def merge_pcap_log():
    log_biflows = shelve.open(PATH+"log.dat") 
    pcap_biflows = shelve.open(PATH+"pcap.dat")
    merged_biflows = shelve.open(PATH+"biflows.dat", writeback=True)

    print('--------------------------------')
    start = datetime.now()
    print('merging bfl')

    try:
        lpm.merge_dict_on_log(pcap_biflows, log_biflows, merged_biflows)
    except:
        print(traceback.format_exc())
        log_biflows.close()
        pcap_biflows.close()
        merged_biflows.close()

    log_biflows.close()
    pcap_biflows.close()
    merged_biflows.close()

    stop = datetime.now()
    t = stop-start
    print('merge done in ' + str(t))

    #mng.compress_file(PATH+"biflows.dat")
    #mng.compress_file(PATH+"log.dat")
    #mng.compress_file(PATH+"pcap.dat")
    pcap = glob(PATH+"*.pcap")
    mng.delete_dir(pcap[0], False)
    mng.delete_dir(PATH+"conn.log.labeled", False)

if __name__ == "__main__":
    if len(sys.argv) == 2:
        #print("FOLDER " + sys.argv[1]) #folder in cui ho .pcap e conn.log.labeled
        PATH = sys.argv[1]
        CAPTURE = PATH.split("/")[1]
        #print(CAPTURE)
        
        log_path = PATH+"conn.log.labeled"
        
        #split log
        #split_log(log_path)
        #parse log
        #parse_log()
        #get biflows from log
        #get_biflows_log()
        #sort log dat
        #sort_log_dat()

        #get biflows from pcap
        #get_biflows_pcap()
        #merge pcap dat
        #merge_pcap_dat()
        #sort pcap dat
        #sort_pcap_dat()
        
        #merge pcap and log
        merge_pcap_log()

    else:
        print("Insert two args. First file log labeled and then file pcap")
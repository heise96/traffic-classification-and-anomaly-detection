import shelve
import json 
import traceback

def conv(file_name):

    try:
        b = shelve.open(file_name, writeback=True)

        for k in b:
            for bfl in b[k]:
                flags = []
                ip = []
                transport = []

                bfl['ts'] = str(bfl['ts'])

                for f in bfl['flags']:
                    flags.append(str(f))
                for i in bfl['ip']:
                    ip.append(str(i))
                for t in bfl['transport']:
                    transport.append(str(t))
                
                bfl['ip'] = ip
                bfl['transport'] = transport
                bfl['flags'] = flags
        
        d = dict(b)

        with open("36.json", "w") as outfile:
            json.dump(d, outfile)

        b.close()
    except:
        print(traceback.format_exc())
        print('closed')
        b.close()

conv('36_sampled.dat')
import shelve
import sys
from datetime import datetime
import traceback

sys.path.insert(1, '../src/util')
sys.path.insert(1, '../src')

import dir_manager as mng
import csv_manager as cm


if __name__ == "__main__":

    if len(sys.argv) == 2:
        print("Dat file: " + sys.argv[1]) 
        dat_file = sys.argv[1]
        biflows = shelve.open('../' + dat_file)
        
        print("starting creating csv")
        start = datetime.now()

        index = 0
        print(dat_file[:len(dat_file)-4] + '.csv')
        with open("../"+dat_file[:len(dat_file)-4] + '.csv', 'w') as out_file:
            
            csv_columns = ['quintuple','ts','detailed_label']
            out_file.writelines(';'.join(csv_columns) + '\n')

            try:
                for quintuple in biflows:
                    index = index + 1
                    if index%5000000 == 0:
                        print("-----QUINTUPLE N."+str(index)+"-------")

                    for flow in biflows[quintuple]:
                        out_file.writelines(quintuple + ';' + str(flow['ts']) + ';' + flow['detailed-label'] + '\n')
            except:
                print(traceback.format_exc())
                biflows.close()

        end = datetime.now()-start
        print("csv created in " + str(end))
        biflows.close()

        #--------------------------------------------------------------

        class_count = {
            "-" : [],
            "C&C" : [],
            "C&C-FileDownload" : [],
            "C&C-HeartBeat" : [],
            "C&C-HeartBeat-Attack" : [],
            "C&C-HeartBeat-FileDownload" : [],
            "C&C-Mirai" : [],
            "C&C-Torii" : [],
            "DDoS" : [],
            "FileDownload" : [],
            "Okiru" : [],
            "Okiru-Attack" : [],
            "PartOfAHorizontalPortScan" : [],
            "PartOfAHorizontalPortScan-Attack" : [],
            "C&C-PartOfAHorizontalPortScan" : [],
            "Attack" : []
        }

        csv_file = dat_file[:len(dat_file)-4] + '.csv'

        print('--------------------------------')
        cm.fill_class_count(csv_file, class_count)
        print('--------------------------------')
        cm.sampling(csv_file, class_count)

        mng.delete_dir("../"+csv_file, False)

    else:
        print("Insert two args")
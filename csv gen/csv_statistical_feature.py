# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import shelve
import traceback
from glob import glob
from pprint import pprint
from datetime import datetime
import numpy as np
from statsmodels import robust
import scipy
import sys

sys.path.insert(1, '../src/util')

import dir_manager as mng

def get_stats(flow, field):
    
    packet_field_series = list(map(float, flow[field]))

    if(field == 'iat'):
        if len(packet_field_series) == 1:
            min=0
        else:
            temp = packet_field_series[1:]
            min = float('%.5f'%(np.min(temp)))
  
    stats = {
        'mean' : float('%.5f'%(np.mean(packet_field_series))),
        'min' : float('%.5f'%(np.min(packet_field_series))) if field != 'iat' else min,
        'max' : float('%.5f'%(np.max(packet_field_series))),
        'std' : float('%.5f'%(np.std(packet_field_series))),
        'var' : float('%.5f'%(np.var(packet_field_series))),
        'mad' : float('%.5f'%(robust.mad(packet_field_series, c=1))),
        'skew' : float('%.5f'%(scipy.stats.skew(packet_field_series, bias=False))),
        'kurtosis' : float('%.5f'%(scipy.stats.kurtosis(packet_field_series, bias=False))),
        '10_percentile' : float('%.5f'%(np.percentile(packet_field_series, 10))),
        '20_percentile' : float('%.5f'%(np.percentile(packet_field_series, 20))),
        '30_percentile' : float('%.5f'%(np.percentile(packet_field_series, 30))),
        '40_percentile' : float('%.5f'%(np.percentile(packet_field_series, 40))),
        '50_percentile' : float('%.5f'%(np.percentile(packet_field_series, 50))),
        '60_percentile' : float('%.5f'%(np.percentile(packet_field_series, 60))),
        '70_percentile' : float('%.5f'%(np.percentile(packet_field_series, 70))),
        '80_percentile' : float('%.5f'%(np.percentile(packet_field_series, 80))),
        '90_percentile' : float('%.5f'%(np.percentile(packet_field_series, 90)))
    }

    return stats


def calculate_pck_rate(iat_array, count):
    if len(iat_array) != 1:
        time = sum(iat_array)
        pck_rate = (count/time)
    else:
        pck_rate = count

    return float('%.5f'%(pck_rate))


if __name__ == "__main__":
    dat_files = glob('../rsc/dat_sampled/*')

    metrics = ['mean','min','max','std','var','mad','skew','kurtosis','10_percentile','20_percentile','30_percentile','40_percentile','50_percentile','60_percentile','70_percentile','80_percentile','90_percentile']
    features = ['pl','iat','ttl','ws']
    csv_columns = []

    for feature in features:
        for metric in metrics:
            csv_columns.append(feature+"_"+metric)
    csv_columns.append('pck_num')
    csv_columns.append('pck_rate')
    csv_columns.append('label')
    csv_columns.append('detailed_label')

    mng.make_dir('../rsc/csv_features')

    for dat_file in dat_files:
        file_name = dat_file.split('/dat_sampled/')[1].split('_sampled.dat')[0]

        biflows = shelve.open(dat_file)
        print("starting creating csv - " + file_name)
        start = datetime.now()

        i = 0
        with open("../rsc/csv_features/" + file_name + ".csv", 'w') as out_file:
            out_file.writelines(','.join(csv_columns) + '\n')

            try:
                for quintuple in biflows:
                    i += 1
                    if i%5000000 == 0:
                        print("-----QUINTUPLE N."+str(i)+"-------")

                    for flow in biflows[quintuple]:
                        pl_stats = get_stats(flow,'pl')
                        iat_stats = get_stats(flow,'iat')
                        ttl_stats = get_stats(flow,'ttl')
                        ws_stats = get_stats(flow,'ws')

                        pl_stats = [str(pl_stats[k]) for k in pl_stats]
                        iat_stats = [str(iat_stats[k]) for k in iat_stats]
                        ttl_stats = [str(ttl_stats[k]) for k in ttl_stats]
                        ws_stats = [str(ws_stats[k]) for k in ws_stats]
                        pck_num = str(len(flow['pd']))
                        pck_rate = str(calculate_pck_rate(flow['iat'],len(flow['pd'])))


                        flow_stats = pl_stats + iat_stats + ttl_stats + ws_stats + [pck_num,pck_rate,flow['label'],flow['detailed-label']]
                        out_file.writelines(','.join(flow_stats)+'\n')
            except:
                print(traceback.format_exc())
                biflows.close()

        end = datetime.now()-start
        print("csv created in " + str(end))
        biflows.close()



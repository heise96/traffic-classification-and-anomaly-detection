from datetime import datetime
from glob import glob
import numpy as np
import re
import shutil

def tryint(s):
    try:
        return int(s)
    except:
        return s

def alphanum_key(s):
    return [ tryint(c) for c in re.split('([0-9]+)', s) ]

def npy_merge(npy_files, out_file):

    cumulative_array = []
    npy_files.sort(key=alphanum_key)

    for npy_file in npy_files:
        print(npy_file)

        npy_data = np.load(npy_file)

        cumulative_array.extend(npy_data)
    
    cumulative_array = np.array(cumulative_array)
    np.save('../' + out_file, cumulative_array)



if __name__ == "__main__":

    print('--------------------------------')
    start = datetime.now()
    print('merging')

    npy_features_files = glob('../npy_header/features/*')
    npy_labels_files = glob('../npy_header/labels/*')

    npy_merge(npy_features_files, 'dataset_features_header.npy')
    npy_merge(npy_labels_files, 'dataset_labels_header.npy')

    stop = datetime.now()
    t = stop-start
    print(' merge done in ' + str(t))


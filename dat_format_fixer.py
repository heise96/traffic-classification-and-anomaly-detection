import shelve 
import traceback
from glob import glob 

max_g = -1
min_g = 256

def convert(in_array, field):
    for data in flow[field]:
        if len(data) > 0:
            data_converted = list(bytearray(data.decode(encoding='latin-1'), encoding='latin-1'))
            if len(data) != len(data_converted):
                raise ValueError('ERROR - len mismatch')

            #range check
            global max_g
            global min_g
            m = max(data_converted)
            mi = min(data_converted)
            if m > max_g:
                max_g = m
            if mi < min_g:
                min_g = mi

            in_array.append(data_converted)
        else:
            in_array.append([0])
    flow[field] = in_array


dat_files = glob('./dat_sampled/*')
for dat_file in dat_files:
    print('---------------------------')
    print(dat_file)
    biflows = shelve.open(dat_file, writeback=True)

    for quintuple in biflows:
        for flow in biflows[quintuple]:
            t_array = []
            i_array = []
            p_array = []
            try:
                convert(t_array, 'transport')
                convert(i_array, 'ip')
                convert(p_array, 'pd')
            except:
                print(traceback.format_exc())
                biflows.close()

    print('max ' + str(max_g))
    print('min ' + str(min_g))

    biflows.close()
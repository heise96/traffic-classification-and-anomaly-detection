
with open('conn.log.labeled', 'w') as out_file:
    with open('conn.log', 'r') as in_file:

        line_count = sum(1 for line in in_file)
        in_file.seek(0)

        for i,line in enumerate(in_file):

            if i == 6:
                nl = line.replace('\n','')
                nl = nl + '   label   detailed-label\n'
            elif i == 7:
                nl = line.replace('\n','')
                nl = nl + '   string   string\n'
            elif i>7 and i != line_count-1:
                nl = line.replace('\n','')
                nl = nl + '   Benign   -\n'
            else:
                nl = line
            
            out_file.writelines(nl)
            

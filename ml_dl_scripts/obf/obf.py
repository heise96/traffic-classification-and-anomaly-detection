from keras.models import model_from_json
import os
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
import sklearn
import imblearn
from sklearn.metrics import confusion_matrix
import matplotlib.cm as cm
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import seaborn as sns
from glob import glob

def make_dir(path):
    try:
        os.mkdir(path)
    except OSError:
        print ("Creation of the directory %s failed" % path)
    else:
        print ("Successfully created the directory %s" % path)

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

#load cnn 576 -no obf- 
model_name = 'dataset_cnn_576'
json_file = open('./model/' + model_name +'.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
model = model_from_json(loaded_model_json)
model.load_weights('./model/' + model_name +'.h5')
print('Loaded model from disk')

#model.summary()

#read datasets with obf
datasets = glob('./576/*')
for dataset in datasets:
    dataset_name = dataset.split('./576/dataset_')[1][:-8]
    make_dir('./res/' + dataset_name)
    
    df = pd.read_parquet(dataset)

    labels = np.array(df['class'])
    labels = labels.astype("int32")
    samples = df.drop('class', axis=1)
    features_list = list(samples.columns)
    samples = np.array(samples)
    classes = np.unique(labels)

    label_encoder = LabelEncoder()
    categorical_labels = label_encoder.fit_transform(labels)

    samples_train, samples_test, categorical_labels_train, categorical_labels_test = \
        train_test_split(samples, categorical_labels, shuffle=True, random_state=0, stratify=categorical_labels)

    samples_train = np.expand_dims(samples_train, axis=2)
    samples_test = np.expand_dims(samples_test, axis=2)

    #testing
    soft_values = model.predict(samples_test, verbose=2)
    prediction_test = soft_values.argmax(axis=-1)

    accuracy = sklearn.metrics.accuracy_score(categorical_labels_test, prediction_test)
    fmeasure = sklearn.metrics.f1_score(categorical_labels_test, prediction_test, average='macro')
    macro_gmean = np.mean(imblearn.metrics.geometric_mean_score(categorical_labels_test, prediction_test, average=None))
    classification_report = sklearn.metrics.classification_report(categorical_labels_test, prediction_test)

    with open('./res/' + dataset_name + '/c_repo.txt', 'w') as c_repo: 
        c_repo.write('Accuracy: {}'.format(accuracy) + '\n')
        c_repo.write('Macro F-measure: {}'.format(fmeasure) + '\n')
        c_repo.write('Macro G-mean: {}'.format(macro_gmean) + '\n')
        c_repo.write(classification_report)

    #data for conf mat
    np.save('./res/' + dataset_name + '/' + dataset_name + '_sf.npy', soft_values) #argmax for prediction_test
    np.save('./res/' + dataset_name + '/' + dataset_name + '_clt.npy', categorical_labels_test)

    mat = confusion_matrix(categorical_labels_test, prediction_test, normalize = 'true')

    plt.figure(figsize=(12, 8), dpi=70)
    cmap = cm.get_cmap('PuBuGn')
    sns.heatmap(mat.T*100, square=True, annot=False, vmin=0.0, vmax=100.0, cmap=cmap, cbar=True)
    sns.set(font_scale=1.4) 
    plt.xlabel('True Label')
    plt.ylabel('Predicted Label')
    plt.title('Confusion Matrix of DL Traffic Classifier')
    plt.savefig('./res/' + dataset_name + '/' + dataset_name)

    plt.figure(figsize=(12, 8), dpi=70)
    sns.heatmap(mat.T*100, square=True, annot=False, vmin=0.0, vmax=100.0, cmap=cmap, cbar=True, norm=LogNorm(vmin=0.01, vmax=100.0)) # plot via seaborn in logscale
    sns.set(font_scale=1.4) 
    plt.xlabel('True Label')
    plt.ylabel('Predicted Label')
    plt.title('Confusion Matrix of DL Traffic Classifier in Log-scale')
    plt.savefig('./res/' + dataset_name + '/' + dataset_name + '_log')


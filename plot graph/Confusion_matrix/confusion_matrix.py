from sklearn.metrics import confusion_matrix
import matplotlib.cm as cm
from matplotlib.colors import LogNorm
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
classes = [0,'',2,'',4,'',6,'',8,'',10,'',12]
model = 'cnn_576'
categorical_labels_test = np.load(model+'_clt.npy')
#print(categorical_labels_test)
prediction_test = np.load(model+'_sf.npy')
if 'mimetic' in model:
	prediction_test= np.argmax(prediction_test[0], axis = 1)
else:
	prediction_test= np.argmax(prediction_test, axis = 1)

mat = confusion_matrix(categorical_labels_test, prediction_test, normalize = 'true')
plt.figure(figsize=(8, 8), dpi=70)
cmap = cm.get_cmap('Oranges')
ax = plt.subplot()
sns.heatmap(mat.T*100, square=True, annot=False, vmin=0.0, vmax=100.0, cmap=cmap, cbar=True,ax=ax,xticklabels=classes, yticklabels=classes, linewidths=0.5, linecolor='black') # plot via seaborn
sns.set(font_scale=1.4) # for label size
plt.xlabel('True Label')
plt.ylabel('Predicted Label')
plt.tight_layout()
plt.figure()

with sns.axes_style("white"):
	ax = plt.subplot()
	sns.set_style("white")
	sns.set_theme(style="whitegrid")
	ax = sns.heatmap(mat.T*100, square=True, annot=False, vmin=0.0, vmax=100.0, cmap=cmap, cbar=True, norm=LogNorm(vmin=0.01, vmax=100.0),ax=ax,xticklabels=classes, yticklabels=classes)
	cbar = ax.collections[0].colorbar
	cbar.ax.tick_params(labelsize=15,length=4)
	cbar.ax.tick_params(which='minor',length=2.5)
	sns.set(font_scale=4) # for label size
	plt.xlabel('True Label',fontsize=20)
	plt.ylabel('Predicted Label',fontsize=20)
	ax.tick_params(axis='both', which='major', labelsize=16)
	ax.tick_params(axis='y', rotation=0, labelsize=16)    
	plt.tight_layout()
	plt.savefig(model+'.pdf', bbox_inches='tight')
from operator import index
import os,sys,glob,getopt
from posixpath import basename
from pathlib import Path
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from seaborn.palettes import color_palette
import plot_library as plib
from matplotlib.ticker import MultipleLocator
classifier_labels_map={
    'kaceto19':'MIMETIC',
    'kwang19cnn':'Wang',
    'klopez19rnn':'Lopez',
    'kwang17':'Wang',
    'klopez17cnnrnn':'Lopez'
}

label_map={
    'a':'app',
    't':'activity',
    'm': 'appactivity'
}
#multimodal_wang2017endtoend_lopez2017network_1DCNN_GRU_test_times.dat

classifiers_map={
    'wang2017endtoend_1DCNN': '1D-CNN',
    'lopez2017network_CNN_RNN_2a':'Hybrid',
    'multimodal_wang2017endtoend_lopez2017network_1DCNN_GRU':'MIMETIC'
}  

classifiers_archs={
    'wang':'wang2017endtoend_1DCNN',
    'lopez':'lopez2017network_CNN_RNN_2a',
    'mimetic':'multimodal_wang2017endtoend_lopez2017network_1DCNN_GRU'
}

label_name_map={
        'a':'APP',
        't':'APP',
        'm': 'APPACT'
}

class_order=['Wang','Lopez','MIMETIC']
def main(argv):
    nf=False
    
    #results_path=sys.argv[1]
    #kind=sys.argv[2]
    #output_dir=sys.argv[3]
    colormap='YlGnBu'
    params=False
    star=None
    try:
        opts, args = getopt.getopt(argv, "hr:k:o:a:C:s:", "[input_config=,classifier=]")
    except getopt.GetoptError:
        print('oreo_run.py -i <input_config> -c <classifier>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print('oreo_run.py -r <RESULTS PATH> -k <KIND (APP, ACT, APPACT)> -a <ARCHITECTURE (wang, lopez, mimetic)> -o <OUTPUT PATH> -C <COLORMAP> -s <STAR (where to plate the star)>')
            sys.exit()
        if opt in ("-r", "--results"):
            results_path= arg
        if opt in ("-k", "--kind"):
            kind = arg
        if opt in ("-a", "--arch"):
            ar = arg
            if ar not in classifiers_archs.keys():
                print('Wrong architecture name (wang,lopez,mimetic)')
                exit(1)
        if opt in ('-o', '--output'):
            output_dir = arg
            #print(output_dir)
            #input()
        if opt in ('-C', '--colormap'):
            colormap = arg
        if opt in ('-s', '--star'):
            star=int(arg)
    Path(output_dir).mkdir(parents=True,exist_ok=True)
    
    files=glob.glob('%s/%s*%s*Sensitivity.parquet' %(results_path,classifiers_archs[ar],kind))
    if len(files)==1:
        print("un file")
        df_res=pd.read_parquet(files[0])
        #print(df_res)
        #df_res.to_csv("wang_original.csv")
		
        #df_res = pd.read_csv("wang.csv")
        #c='wang'
        df_res = pd.read_csv("lopez.csv")
        c='Hybrid'
    elif len(files)>1:
        df_res=pd.read_parquet(files[0])
        for file in files[1:]:
            df_temp=pd.read_parquet(file)
            df_res=pd.concat([df_res,df_temp],axis=0)
    else:
        print('Files not fond')
        exit()
        
    print(df_res)
    #cl=['wang' if classifiers_archs['wang'] in os.path.basename(file) else 'lopez' if classifiers_archs['lopez'] in os.path.basename(file) else 'aceto' if classifiers_archs['mimetic'] in os.path.basename(file) else 'unk' for file in files ]       
    #c=cl[0] if len(np.unique(cl))==1 else 'mixed'
    #c=ar
    #c='lopez'
    fmetrics=['Accuracy','F-measure','G-Mean']
    #df_res['Classifier']=df_res['Classifier'].apply(lambda x: classifier_labels_map[x])
    metrics=['Accuracy','F1 Score','G-Mean']
    metrics_h=['hAccuracy','hF1 Score']
    metrics_l=['lAccuracy','lF1 Score']


    df_exp=pd.DataFrame(columns=['Classifier','NPkts','NByte','Value','Fold','Metric','Params'])   
    df_exp_h=pd.DataFrame(columns=['Classifier','NPkts','NByte','Value','Fold','Metric'])   
    df_exp_l=pd.DataFrame(columns=['Classifier','NPkts','NByte','Value','Fold','Metric'])  
    
    for i,row in df_res.iterrows():
        clas=row['Classifier']
        fo=row['Fold']
        for m in metrics:
            v=row[m]
            df_exp=df_exp.append({'Classifier':clas,'NPkts':int(row['NPkts']),'NByte':int(row['NByte']),'Value':v,'Fold':fo,'Metric':'F-measure' if m=='F1 Score' else m},ignore_index=True)
           
        if kind=='APPACT': 
            for m in metrics_h:
                v=row[m]
                df_exp_h=df_exp_h.append({'Classifier':clas,'NPkts':int(row['NPkts']),'NByte':int(row['NByte']),'Value':v,'Fold':fo,'Metric':'F-measure' if m=='hF1 Score' else m.replace('h','')},ignore_index=True)
            for m in metrics_l:
                v=row[m]
                df_exp_l=df_exp_l.append({'Classifier':clas,'NPkts':int(row['NPkts']),'NByte':int(row['NByte']),'Value':v,'Fold':fo,'Metric':'F-measure' if m=='lF1 Score' else m.replace('l','')},ignore_index=True)
      
    metrics_cols = [
        'Configuration',
        'Classifier',
        'LabelType',
        'NPkts',
        'NByte',
        'params',
        'Fold',
        'Accuracy',
        'F1 Score',
        'hAccuracy',
        'hF1 Score',
        'lAccuracy',
        'lF1 Score'
    ]
    
    #qui
    for classx in df_res['Classifier'].unique():
        #classx='Hybrid'
        param_field='NPkts' if classx=='Hybrid' else 'NByte'
        #print(classx)
        ps=df_res[df_res['Classifier']==classx][param_field].values
        nps=df_res[df_res['Classifier']==classx]['params'].values
        print(nps)
        for p,n in zip(ps,nps):
            df_exp.loc[df_exp[param_field]==p,'Params']=int(n)/1000
            if kind=='APPACT':
                df_exp_h.loc[df_exp_h[param_field]==p,'Params']=int(n)/1000   
                df_exp_l.loc[df_exp_l[param_field]==p,'Params']=int(n)/1000
                
        df_expc=df_exp.groupby('Classifier').get_group(classx)
        
        colors=sns.color_palette("Blues",n_colors=len(fmetrics)+4)#Blues
        #colors=sns.color_palette("ch:start=.2,rot=-.3", as_cmap=True)
        met_palette={}
        for i,met in enumerate(fmetrics):
                print(colors[i])
                met_palette[met]=colors[i+3]
        f,ax=plt.subplots(1,1, figsize=(12, 3.5))
        ax.set_ylim(15,100)#15,100
        savefig=os.path.join(output_dir,'%s_Line_Metrics_App_with_Activities.pdf'%classx if kind in ['MLB','AppActivity','APPACT'] else '%s_Metrics_App.pdf'%classx)
        sns.lineplot(ax=ax,x=param_field,y='Value',hue='Metric',style='Metric',data=df_expc[df_expc['Metric'].isin(fmetrics)],palette=met_palette, markers=['^','s','o'], lw=3,ms=9,legend='auto',zorder=1)
        handles1, labels1 = ax.get_legend_handles_labels()
        for handle in handles1:
            handle.set_linewidth(3)
            handle.set_markersize(9)
        #plib.df_bar_plot(ax=ax,x='Metric',y='Value',data=df_expc,hue=param_field,ylim=(0,1),ylabel='%',palette='viridis')
        ax.set_xticks(df_expc[param_field].unique().astype(int))
        ax.grid(which='major',axis='x',alpha=0.5,ls='--')
        ax.set_xlabel('#packets' if c=='Hybrid' else '#bytes' if c=='wang' else '?',fontsize=18)
        ax.set_ylabel('%',fontsize=18)
        ax.tick_params(axis='x',labelsize=18)
        ax.tick_params(axis='y',labelsize=18)
        ax.yaxis.set_minor_locator(MultipleLocator(5))
        ax.yaxis.set_major_locator(MultipleLocator(20))

                    
        ax2=ax.twinx()
        sns.lineplot(ax=ax2,x=param_field,y='Params',data=df_expc[df_expc['Metric'].isin(fmetrics)],color='k', marker='D', lw=3, ls='--', ms=9, label='# TP',zorder=2)
        mx=df_expc[df_expc['Metric']==fmetrics[0]].groupby(param_field).mean()
            
        if star:
            sns.scatterplot(x=[star],y=21,color=met_palette[met],facecolor='orange', marker='*',edgecolor='orange', zorder=300,s=350,ax=ax)
        else:
            mx=df_expc[df_expc['Metric']=='F-measure'].groupby(param_field).mean()   
            mx['tdoff']=mx.apply(lambda x: x['Value']/x['Params'], axis=1)
            sns.scatterplot(x=[mx['tdoff'].idxmax()],y=50,color=met_palette[met],facecolor='orange', marker='*',edgecolor='orange', zorder=300,s=300,ax=ax)
                
        #ax2.legend()
        ax2.set_ylabel('#TP [k]',fontsize=18)
        ax2.set_ylim(0 if classx=='Hybrid' else 0,100000 if classx=='Hybrid' else 1900)#1050 or 2400
        handles2, labels2 = ax2.get_legend_handles_labels()
        ax.legend(handles1+handles2, labels1+labels2,ncol=4,frameon=False,markerscale=0.85,fontsize=18,handlelength=3,loc='upper center',bbox_to_anchor=(0., 1.2, 1., .102))
        ax2.legend().remove()
        ax2.tick_params(axis='y',labelsize=18)
        ax2.yaxis.set_minor_locator(MultipleLocator(125 if classx=='Hybrid' else 100))#50 or 100
        ax2.yaxis.set_major_locator(MultipleLocator(500 if classx=='Hybrid' else 500))#250 or 500
                        
        
        plt.tight_layout()
        plt.savefig(savefig, format='pdf', bbox_inches='tight', pad_inches=0.0)
        plt.close()
        
        
        if kind=='APPACT': 
            
            #APP
            df_expc_h=df_exp_h.groupby('Classifier').get_group(classx)
        
            f,ax=plt.subplots(1,1, figsize=(12, 3.5))
            ax.set_ylim(90,100)
            savefig=os.path.join(output_dir,'%s_Line_Metrics_App_from_APPACT.pdf'%classx)
            sns.lineplot(ax=ax,x=param_field,y='Value',hue='Metric',style='Metric',data=df_expc_h[df_expc_h['Metric'].isin(fmetrics)],palette=met_palette, markers=['^','s','o'], lw=3,ms=8,legend='auto',zorder=1)
            handles1, labels1 = ax.get_legend_handles_labels()
            for handle in handles1:
                handle.set_linewidth(3)
                handle.set_markersize(10)

            ax.set_xticks(df_expc_h[param_field].unique().astype(int))
            ax.grid(which='major',axis='x',alpha=0.5,ls='--')
            ax.set_xlabel('#packets' if c=='lopez' else '#bytes' if c=='wang' else '?',fontsize=18)
            ax.set_ylabel('%',fontsize=18)
            ax.tick_params(axis='x',labelsize=18)
            ax.tick_params(axis='y',labelsize=18)
            ax.yaxis.set_minor_locator(MultipleLocator(2.5))

                        
            ax2=ax.twinx()
            sns.lineplot(ax=ax2,x=param_field,y='Params',data=df_expc_h[df_expc_h['Metric'].isin(fmetrics)],color='k', marker='D', lw=3, ls='--', ms=12, label='# TP',zorder=2)
            mx=df_expc_h[df_expc_h['Metric']==fmetrics[0]].groupby(param_field).mean()
                
            if star:
                sns.scatterplot(x=[star],y=43,color=met_palette[met],facecolor='orange', marker='*',edgecolor='orange', zorder=300,s=350,ax=ax)
            else:
                mx=df_expc_h[df_expc_h['Metric']=='F-measure'].groupby(param_field).mean()   
                mx['tdoff']=mx.apply(lambda x: x['Value']/x['Params'], axis=1)
                sns.scatterplot(x=[mx['tdoff'].idxmax()],y=50,color=met_palette[met],facecolor='orange', marker='*',edgecolor='orange', zorder=300,s=300,ax=ax)
                    
            ax2.set_ylabel('#TP [k]',fontsize=18)
            ax2.set_ylim(0 if classx=='Hybrid' else 0,100000 if classx=='Hybrid' else 16000)
            handles2, labels2 = ax2.get_legend_handles_labels()
            ax.legend(handles1+handles2, labels1+labels2,ncol=4,frameon=False,markerscale=0.85,fontsize=18,handlelength=3,loc='upper center',bbox_to_anchor=(0., 1.2, 1., .102))
            ax2.legend().remove()
            ax2.tick_params(axis='y',labelsize=18)
            ax2.yaxis.set_minor_locator(MultipleLocator(125 if classx=='Hybrid' else 1000))
            ax2.yaxis.set_major_locator(MultipleLocator(250 if classx=='Hybrid' else 4000))
                            
            
            plt.tight_layout()
            plt.savefig(savefig, format='pdf', bbox_inches='tight', pad_inches=0.0)
            plt.close()    
            
            
            
            #ACT
            df_expc_h=df_exp_l.groupby('Classifier').get_group(classx)
        
            f,ax=plt.subplots(1,1, figsize=(12, 3.5))
            ax.set_ylim(15,80)
            savefig=os.path.join(output_dir,'%s_Line_Metrics_Activity_from_APPACT.pdf'%classx)
            sns.lineplot(ax=ax,x=param_field,y='Value',hue='Metric',style='Metric',data=df_expc_h[df_expc_h['Metric'].isin(fmetrics)],palette=met_palette, markers=['^','s','o'], lw=3,ms=12,legend='auto',zorder=1)
            handles1, labels1 = ax.get_legend_handles_labels()
            for handle in handles1:
                handle.set_linewidth(3)
                handle.set_markersize(10)

            ax.set_xticks(df_expc_h[param_field].unique().astype(int))
            ax.grid(which='major',axis='x',alpha=0.5,ls='--')
            ax.set_xlabel('#packets' if c=='lopez' else '#bytes' if c=='wang' else '?',fontsize=18)
            ax.set_ylabel('%',fontsize=18)
            ax.tick_params(axis='x',labelsize=18)
            ax.tick_params(axis='y',labelsize=18)
            ax.yaxis.set_minor_locator(MultipleLocator(5))

                        
            ax2=ax.twinx()
            sns.lineplot(ax=ax2,x=param_field,y='Params',data=df_expc_h[df_expc_h['Metric'].isin(fmetrics)],color='k', marker='D', lw=3, ls='--', ms=12, label='# TP',zorder=2)
            mx=df_expc_h[df_expc_h['Metric']==fmetrics[0]].groupby(param_field).mean()
                
            if star:
                sns.scatterplot(x=[star],y=43,color=met_palette[met],facecolor='orange', marker='*',edgecolor='orange', zorder=300,s=350,ax=ax)
            else:
                mx=df_expc_h[df_expc_h['Metric']=='F-measure'].groupby(param_field).mean()   
                mx['tdoff']=mx.apply(lambda x: x['Value']/x['Params'], axis=1)
                sns.scatterplot(x=[mx['tdoff'].idxmax()],y=50,color=met_palette[met],facecolor='orange', marker='*',edgecolor='orange', zorder=300,s=300,ax=ax)
                    
            ax2.set_ylabel('#TP [k]',fontsize=18)
            ax2.set_ylim(0 if classx=='Hybrid' else 0,100000 if classx=='Hybrid' else 16000)
            handles2, labels2 = ax2.get_legend_handles_labels()
            ax.legend(handles1+handles2, labels1+labels2,ncol=4,frameon=False,markerscale=0.85,fontsize=18,handlelength=3,loc='upper center',bbox_to_anchor=(0., 1.2, 1., .102))
            ax2.legend().remove()
            ax2.tick_params(axis='y',labelsize=18)
            ax2.yaxis.set_minor_locator(MultipleLocator(125 if classx=='Hybrid' else 1000))
            ax2.yaxis.set_major_locator(MultipleLocator(250 if classx=='Hybrid' else 4000))
                            
            
            plt.tight_layout()
            plt.savefig(savefig, format='pdf', bbox_inches='tight', pad_inches=0.0)
            plt.close()   

    
if __name__=='__main__':
    main(sys.argv[1:])    
      
    
import numpy as np

import seaborn as sns
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
from statsmodels.distributions.empirical_distribution import ECDF
from matplotlib.ticker import MultipleLocator, FormatStrFormatter, AutoMinorLocator
import statsmodels.api as sm
from statsmodels.graphics.gofplots import qqline
from matplotlib.offsetbox import AnchoredText

def add_average_to_barplot(df, feature,order):
                        for fold in df.Fold.unique():
                            for metric in df.Metric.unique():
                                for type in df.Type.unique():
                                    df = df.append(
                                        {
                                            'App': 'Average',
                                            'Fold': fold,
                                            'Order': order,
                                            'nbins': 0,
                                            'test_BFs_%s' % feature: 0,  # row_temp['test_MEAN_GMean_BASE'],
                                            'test_PKTs_%s' % feature: df.groupby(['Fold', 'Metric', 'Type']).get_group(
                                                (fold, metric, type))['test_PKTs_%s' % feature].mean(),
                                            'Metric': metric,
                                            'Type': type
                                        }, ignore_index=True
                                    )
                        return df

def df_bar_plot(ax, x, y, data,hue=None, hue_order=None,  
                xmajor_loc=None, xminor_loc=None,
                ymajor_loc=None, yminor_loc=None,
                xlabel=None, ylabel=None, xlim=None, ylim=None,
                scale=None, palette=None,
                savepath=None, format='pdf', 
                decimal=False, annotation=None):

    bar=sns.barplot(x=x, y=y, hue=hue,hue_order=hue_order, data=data,ax=ax, palette=palette)
    
    ax.grid(b=True,linestyle='--', linewidth=0.5, alpha=0.5, zorder=1, axis='y')
    
    if xlabel:
        ax.set_xlabel(xlabel, fontsize=20)
    if ylabel:
        ax.set_ylabel(ylabel, fontsize=20)
    dec=1 if decimal else 0
    if scale:
        if decimal:
            ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: '{:.1f}'.format(x/scale)))
        else:
            ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: '{:.0f}'.format(x/scale)))

    if annotation:
        for i,p in enumerate(bar.patches):
            bar.annotate(annotation[i], 
                        (p.get_x() + p.get_width() / 2., p.get_height()), 
                        ha = 'center', va = 'center', 
                        size=11,
                        xytext = (0, 5), 
                        textcoords = 'offset points')

    
    ax.tick_params(axis='both', labelsize=18)

    if xmajor_loc:
        ax.xaxis.set_major_locator(MultipleLocator(xmajor_loc))
    
    if ymajor_loc:
        ax.yaxis.set_major_locator(MultipleLocator(ymajor_loc))

    if xminor_loc:
        ax.xaxis.set_minor_locator(MultipleLocator(xminor_loc))
    if yminor_loc:
        ax.yaxis.set_minor_locator(MultipleLocator(yminor_loc))
    
    if xlim:
        ax.set(xlim=xlim)
    if ylim:
        ax.set(ylim=ylim)

    if savepath:
        plt.tight_layout()
        plt.savefig(savepath, format=format, bbox_inches='tight', pad_inches=0.05)
    
def plotECDF(data_in, ax, xlabel=None, ylabel=None, x2label=None, label=None, survival=None, color=None, linestyle="solid",
             xscale="lin", yscale="lin", xlim=None, x2lim=None, ylim=(0, 1), majorxTicks=None, extendLeft=0, split=False, split_colors=['r','b'],
             savefig=None, format="pdf", return_statistics=False):
    #    if color == None:
    #        plt.rc('lines', linewidth=4)
    #        plt.rc('axes', prop_cycle=(cycler('color', ['r', 'g', 'b', 'y', 'k', 'm'])))
    #
    #            +cycler('linestyle', ['-', '--', ':', '-.'])))

    # data = list(data)
    data = data_in.copy()
    data.sort()

    # Remove traces with duration < 10 seconds
    #    data = [x for x in data if x >= 10]

    ecdf = ECDF(data)
    y = ecdf(data) if not survival else 1 - ecdf(data)
    y = list(y)

    #    print len(data)
    #    print len(y)
    #    if addZeroZero:
    #        data.insert(0, 0)
    #        y = list(y)
    #        y.insert(0, 0)

    #    print len(data)
    #    print len(y)

    if extendLeft is not None:
        data.insert(0, extendLeft)  # value (with null probability)
        if not survival:
            y.insert(0, 0.0)  # probability
        else:
            y.insert(0, 1.0)  # probability (CCDF)


    if split:
        y_pos=[np.nan if d<= 0 else p for d,p in zip(data,y)]
        y_neg=[np.nan if d>=0 else p for d,p in zip(data,y)]

        ax.step(data, y_pos, label=label, color=split_colors[0], linestyle=linestyle, linewidth=2, where="post")
        ax.step(data, y_neg, label=label, color=split_colors[1], linestyle=linestyle, linewidth=2, where="post")
    else:
    
        if color is None:
            ax.step(data, y, label=label, linestyle=linestyle, linewidth=1, where="post")
        else:
            ax.step(data, y, label=label, color=color, linestyle=linestyle, linewidth=1, where="post")



    #    ax.plot(data, y, label=label, color=color, linestyle=linestyle)

    #    ax.plot(np.percentile(data, 5), 0.05, 'Dm')
    #    ax.plot(np.percentile(data, 95), 0.95, 'Dm')
    #    ax.plot(np.percentile(data, 50), 0.50, 'Dm')
    #    ax.plot(np.mean(data), y[np.abs(data-np.mean(data)).argmin()], 'Dm')

    #    ax.axvspan(np.percentile(data, 5), np.percentile(data, 95), linewidth=0, alpha=0.20, color='grey')

    if return_statistics:
        stats_dict={
                    '5-percentile':np.percentile(data, 5),
                    '95-percentile':np.percentile(data, 95),
                    'Median':np.percentile(data, 50),
                    'Mean': np.mean(data)
        }
    else:
        print('5-percentile: %.4f' % np.percentile(data, 5))
        print('95-percentile: %.4f' % np.percentile(data, 95))
        print('Median: %.4f' % np.percentile(data, 50))
        print('Mean: %.4f' % np.mean(data))

    ax.yaxis.grid(True, ls='--')

    if ylabel:
        ylabel=ylabel
    else:
        ylabel = "CDF" if not survival else "CCDF"

    ax2 = None

    if ylabel:
        ax.set_ylabel(ylabel, fontsize=16)
    if xlabel:
        ax.set_xlabel(xlabel, fontsize=16)
    if x2label:
        ax2 = ax.twiny()
        ax2.set_xlabel(x2label, fontsize=16)

    if xscale == "log":
        ax.set_xscale("log")
    #        ax.set_xticks([i*10**exp for exp in range(-4, 2) for i in range(1, 10)], minor=True)
    #        ax.set_xticks([10**exp for exp in range(-4, 3) ], minor=False)

    if yscale == "log":
        ax.set_yscale("log")
    #        ax.set_yticks([i*10**exp for exp in range(-1, 9) for i in range(1, 1)], minor=True)

    if xscale == "symlog":
        ax.set_xscale("symlog")

    if majorxTicks:
        ax.set_xticks(majorxTicks, minor=False)

    ax.minorticks_on()
    #    ax.tick_params(axis='x',which='minor',top='off')
    #    ax.tick_params(axis='x',which='major',top='off')
    #    ax.tick_params(axis='y',which='major',right='off')
    #    ax.tick_params(axis='y',which='minor',right='off')
    ax.tick_params(axis='x', which='minor', top=False)
    ax.tick_params(axis='x', which='major', top=False, labelsize=14)
    ax.tick_params(axis='y', which='major', right=False, labelsize=14)
    ax.tick_params(axis='y', which='minor', right=False)

    majorFormatter = FormatStrFormatter('%g')
    #    majorFormatter = FormatStrFormatter('%.2f')
    ax.xaxis.set_major_formatter(majorFormatter)
    ax.yaxis.set_major_formatter(majorFormatter)

    if xlim:
        ax.set(xlim=xlim)
    else:
        #ax.set(xlim=(min(data), max(data)))

        #print('Nada')

        '''
        if min(data)>0:
            orders=np.asarray([10**e for e in np.arange(-10,11,dtype=float)])
            xmin=orders[orders<min(data)][-1]
            xmax=orders[orders>max(data)][0]
            ax.set(xlim=(xmin, xmax))
        else:
            ax.set(xlim=(min(data), max(data)))
        '''
        
    if x2lim:
        if ax2 is None:
            ax2 = ax.twiny()
        ax2.set(xlim=x2lim)

        ax2.minorticks_on()
        ax2.tick_params(axis='x', which='minor', top=True)
        ax2.tick_params(axis='x', which='major', top=True, labelsize=14)

        ax2.xaxis.set_major_formatter(majorFormatter)

    if ylim:
        ax.set(ylim=ylim)

    if label is not None:
        ax.legend(loc='lower right', fontsize=16) if not survival else ax.legend(loc="upper right", fontsize=16)

    if savefig:
        plt.tight_layout()
        plt.savefig(savefig, format=format, bbox_inches='tight', pad_inches=0.05)

    if return_statistics:
        return {    '5p':np.percentile(data, 5),
                    '95p':np.percentile(data, 95),
                    'Median':np.percentile(data, 50),
                    'Mean': np.mean(data)}
    else:
        print('5-percentile: %.4f' % np.percentile(data, 5))
        print('95-percentile: %.4f' % np.percentile(data, 95))
        print('Median: %.4f' % np.percentile(data, 50))
        print('Mean: %.4f' % np.mean(data))

def plot_regression_results(ax, y_true, y_pred, xlabel=None, color=None, ylabel=None, xlim=None, ylim=None,
                            majorxTicks=None, title=None, scores=None, elapsed_time=None, savefig=None, format="pdf"):
    """
    Scatter plot of the predicted vs true targets.
    """
    ax.plot([y_true.min(), y_true.max()],
            [y_true.min(), y_true.max()],
            '--r', linewidth=2)
    ax.scatter(y_true, y_pred, s=2.0, alpha=0.2, color=color)

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    ax.spines['left'].set_position(('outward', 10))
    ax.spines['bottom'].set_position(('outward', 10))

    if xlim:
        ax.set(xlim=xlim)
    else:
        ax.set_xlim([y_true.min(), y_true.max()])
    if ylim:
        ax.set(ylim=ylim)
    else:
        ax.set_ylim([y_true.min(), y_true.max()])

    if ylabel:
        ax.set_ylabel(ylabel, fontsize=14)
    else:
        ax.set_xlabel('Actual', fontsize=14)
    if xlabel:
        ax.set_xlabel(xlabel, fontsize=14)
    else:
        ax.set_ylabel('Predicted', fontsize=14)

    if majorxTicks:
        ax.set_xticks(majorxTicks, minor=False)

    ax.minorticks_on()
    #    ax.tick_params(axis='x',which='minor',top='off')
    #    ax.tick_params(axis='x',which='major',top='off')
    #    ax.tick_params(axis='y',which='major',right='off')
    #    ax.tick_params(axis='y',which='minor',right='off')
    ax.tick_params(axis='x', which='minor', top=False)
    ax.tick_params(axis='x', which='major', top=False, labelsize=12)
    ax.tick_params(axis='y', which='major', right=False, labelsize=12)
    ax.tick_params(axis='y', which='minor', right=False)

    majorFormatter = FormatStrFormatter('%g')
    #    majorFormatter = FormatStrFormatter('%.2f')
    ax.xaxis.set_major_formatter(majorFormatter)
    ax.yaxis.set_major_formatter(majorFormatter)

    extra = plt.Rectangle((0, 0), 0, 0, fc="w", fill=False,
                          edgecolor='none', linewidth=0)
    ax.legend([extra], [scores], loc='upper left')
    title = title + '\n Evaluation in {:.2f} seconds'.format(elapsed_time)
    ax.set_title(title)

    if savefig:
        plt.tight_layout()
        plt.savefig(savefig, format=format, bbox_inches='tight', pad_inches=0.05)


def jointPlot(data, x='test_n_packets_BF', y='test_RMSE_BF_distr_PL', xlabel=None, ylabel=None,
              color=None, xscale="lin", yscale="lin", xlim=None, ylim=None, majorxTicks=None, majoryTicks=None,
              outside=False, ncol=1, savefig=None, format="pdf"):
    """
    Joint-plot of RMSE vs. BF/UF/DF length
    """
    g = sns.jointplot(x, y, data=data, kind='hex', stat_func=None, color=None, height=6, ratio=6, space=0,
                      dropna=True, xlim=None, ylim=None, joint_kws=None, marginal_kws=None)
    g.plot_joint(sns.scatterplot, color=color, alpha=0.3, edgecolor=None, marker='.', s=5.0)

    # sns.kdeplot(data=data[x], ax=g.ax_marg_x, shade=True)
    # sns.kdeplot(data=data[y], ax=g.ax_marg_y, shade=True, vertical=True)

    if xlabel and ylabel:
        g.set_axis_labels(xlabel=xlabel, ylabel=ylabel, fontsize=16)
    elif xlabel:
        g.set_axis_labels(xlabel=xlabel, fontsize=16)
    elif ylabel:
        g.set_axis_labels(ylabel=ylabel, fontsize=16)

    if xscale == "log":
        g.ax_joint.set_xscale("log")

    if yscale == "log":
        g.ax_joint.set_yscale("log")

    g.ax_joint.xaxis.grid(True, ls='--')
    g.ax_joint.yaxis.grid(True, ls='--')

    g.ax_joint.minorticks_on()
    g.ax_joint.tick_params(axis='x', which='minor', top=False, direction='out')
    g.ax_joint.tick_params(axis='x', which='major', top=False, direction='out', labelsize=14)
    g.ax_joint.tick_params(axis='y', which='major', right=False, direction='out', labelsize=14)
    g.ax_joint.tick_params(axis='y', which='minor', right=False, direction='out')

    if xlim:
        g.ax_joint.set(xlim=xlim)
    else:
        g.ax_joint.set(xlim=(min(data[x]), max(data[x])))

    if ylim:
        g.ax_joint.set(ylim=ylim)

    # g.ax_marg_x.get_legend().remove()
    # g.ax_marg_y.get_legend().remove()

    # if outside:
    #     g.ax_joint.legend(loc='upper center', frameon=True, bbox_to_anchor=(0.5, -0.11), shadow=False, ncol=4,
    #                       columnspacing=1, fontsize=10)
    # else:
    #     handles, labels = g.ax_joint.get_legend_handles_labels()
    #     g.ax_joint.legend(handles=handles[1:], labels=labels[1:], loc="lower right", frameon=True, handletextpad=0.1,
    #                       ncol=ncol, markerscale=1.5, fontsize=18)

    majorFormatter = FormatStrFormatter('%g')
    g.ax_joint.xaxis.set_major_formatter(majorFormatter)
    g.ax_joint.yaxis.set_major_formatter(majorFormatter)

    # plt.yticks(majoryTicks,
    #            [r'1 \textmu{}s', r'10 \textmu{}s', r'100 \textmu{}s',
    #             '1 ms', '10 ms', '100 ms', '1 s', '10 s', '100 s', '1000 s'])

    if majorxTicks:
        g.ax_joint.set_xticks(majorxTicks, minor=False)
        g.ax_marg_x.set_xticks(majorxTicks, minor=False)

    if majoryTicks:
        g.ax_joint.set_yticks(majoryTicks, minor=False)
        g.ax_marg_y.set_yticks(majoryTicks, minor=False)

    # base = range(2, 10, 1)
    # locations = base[:]
    # mults = [10, 10 ** 2, 10 ** 3, 10 ** 4, 10 ** 5, 10 ** 6, 10 ** 7, 10 ** 8, 10 ** 9]
    # for i in mults:
    #     locations.extend([x * i for x in base])
    #
    # minor_locator = FixedLocator(10 * np.log10(locations))
    # g.ax_joint.yaxis.set_minor_locator(minor_locator)

    if savefig:
        plt.tight_layout()
        g.savefig(savefig, format=format, bbox_inches='tight', pad_inches=0.05)



def df_error_plot(ax, x, y, data,hue=None, hue_order=None,  
                xmajor_loc=None, xminor_loc=None,
                ymajor_loc=None, yminor_loc=None,
                xlabel=None, ylabel=None, xlim=None, ylim=None,
                scale=None, style=None, markers=None, dashes=None, palette=None,
                savepath=None, format='pdf', 
                alpha=.9,text=None, 
                scatter=False, df_scatter=None,x_scatter=None,y_scatter=None,hue_scatter=None,scatter_markers=None,decimal=False):


    #mostro le linee della griglia sia in orrizzontale che in verticale
    #ax.yaxis.grid(True,linestyle='--', linewidth=0.5, zorder=1) # Show the horizontal gridlines
    #ax.xaxis.grid(True) # Show the vertical gridlines
    
    #ax=sns.pointplot(x=x, y=y, hue=hue, hue_order=hue_order, data=data, join=False, scale=0.5, dodge=0.25 , ci='sd', zorder=10, markers=markers, palette=palette)
    ax=sns.lineplot(x=x, y=y, hue=hue, style=style, hue_order=hue_order, data=data , ci='sd', markers=markers, palette=palette, alpha=alpha, dashes=dashes,lw=2.5, ms=10, markeredgecolor=None)
    
    #xt=np.array(np.unique(data['%s'%x].values),dtype=int)
    #xt=np.unique(data['%s'%x].values)
    #plt.xticks(xt,xt)
    #ax.set_xticklabels(xt, fontsize=9)
    #ax.set(xticks=xt)
    #ax.legend(loc='upper left', bbox_to_anchor=(1,1),ncol=1, handletextpad=0.1, columnspacing=0.1)
    #sns.set_style("whitegrid")
    if scatter:
        ax=sns.scatterplot(x=x_scatter, y=y_scatter,hue=hue_scatter,style=hue_scatter, data=df_scatter, s=200,markers=scatter_markers)
        
    ax.grid(b=True,linestyle='--', linewidth=0.5, alpha=0.5, zorder=1)

    if xlabel:
        ax.set_xlabel(xlabel, fontsize=20)
    if ylabel:
        ax.set_ylabel(ylabel, fontsize=20)
    dec=1 if decimal else 0
    if scale:
        if decimal:
            ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: '{:.1f}'.format(x/scale)))
        else:
            ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: '{:.0f}'.format(x/scale)))
    if text:
        anchored_text = AnchoredText(text, loc='lower center')
        ax.add_artist(anchored_text)

    #legend handle ms=10
    #legend handle lw=2.5
    label_ticksize = 16
    #ax.minorticks_on()

    ax.tick_params(axis='both', labelsize=18)

    
    if xmajor_loc:
        ax.xaxis.set_major_locator(MultipleLocator(xmajor_loc))
    
    if ymajor_loc:
        ax.yaxis.set_major_locator(MultipleLocator(ymajor_loc))

    if xminor_loc:
        ax.xaxis.set_minor_locator(MultipleLocator(xminor_loc))
    if yminor_loc:
        ax.yaxis.set_minor_locator(MultipleLocator(yminor_loc))
    
    if xlim:
        ax.set(xlim=xlim)
    if ylim:
        ax.set(ylim=ylim)

    if hue:
        handles, labels = ax.get_legend_handles_labels()
        idrem=[i for i,l in enumerate(labels) if l==hue or l==hue_scatter]
        labels=[l for i,l in enumerate(labels) if i not in idrem]
        handles=[l for i,l in enumerate(handles) if i not in idrem]

        #handles=[ha.set_linewidth(2.5) for ha in handles]
        #handles=[ha.set_markersize(10) for ha in handles]
        leg=ax.legend(handles=handles, labels=labels, ncol=2, loc='upper right', fontsize=18, markerscale=1.5)
        for line in leg.get_lines():
            line.set_linewidth(2.5)
        leg.get_frame().set_linewidth(0.0)
    else:
        ax.legend().set_visible(False)

    plt.tight_layout()

    if savepath:
       plt.savefig(savepath, format=format, bbox_inches='tight', pad_inches=0.0)
    #else:
    #    plt.show()

# https://www.statsmodels.org/devel/generated/statsmodels.graphics.gofplots.qqplot_2samples.html
def qqplot(actual_samples, synthetic_samples, ax, color=None, line=None,
           xlabel=None, ylabel=None, scale=None, xlim=None, ylim=None, xmajor_loc=None, ymajor_loc=None,
           savefig=None, format="pdf", dpi=None):
    if not color:
        color = 'blue'

    fit = False
    if len(synthetic_samples) >= len(actual_samples):
        pp_x = sm.ProbPlot(actual_samples, fit=fit)
        pp_y = sm.ProbPlot(synthetic_samples, fit=fit)

        if xlabel:
            final_xlabel = xlabel
        else:
            final_xlabel = 'Actual Samples'

        if ylabel:
            final_ylabel = ylabel
        else:
            final_ylabel = 'Synthetic Samples'

    else:
        pp_x = sm.ProbPlot(synthetic_samples, fit=fit)
        pp_y = sm.ProbPlot(actual_samples, fit=fit)

        if ylabel:
            final_xlabel = ylabel
        else:
            final_xlabel = 'Synthetic Samples'

        if xlabel:
            final_ylabel = xlabel
        else:
            final_ylabel = 'Actual Samples'

    pp_x.qqplot(xlabel=final_xlabel, ylabel=final_ylabel, line=None, other=pp_y, ax=ax,
                markersize=1.0, color=color, zorder=3)
    qqline(ax, line, x=None, y=None, dist=None, fmt='k--')

    if xlim:
        ax.set(xlim=xlim)
    if ylim:
        ax.set(ylim=ylim)

    if scale:
        # ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: '{:.1f}'.format(x / scale)))
        #ax.xaxis.set_major_formatter(FuncFormatter(lambda x, pos: '{:.1f}'.format(int(x / scale))))
        ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: '{:.1f}'.format(int(x / scale))))

    fontsize = 16
    ax.set_xlabel(final_xlabel, fontsize=fontsize)
    ax.set_ylabel(final_ylabel, fontsize=fontsize)

    if xmajor_loc:
        ax.xaxis.set_major_locator(MultipleLocator(xmajor_loc))
    if ymajor_loc:
        ax.yaxis.set_major_locator(MultipleLocator(ymajor_loc))

    label_ticksize = 14
    ax.minorticks_on()
    ax.tick_params(axis='x', which='minor', top=False)
    ax.tick_params(axis='x', which='major', top=False, labelsize=label_ticksize)
    ax.tick_params(axis='y', which='major', right=False, labelsize=label_ticksize)
    ax.tick_params(axis='y', which='minor', right=False)

    if savefig:
        plt.tight_layout()
        plt.savefig(savefig, format=format, bbox_inches='tight', pad_inches=0.05, dpi=dpi)



def plot_viterbi_state_sequence(actual, predicted, state_sequence, ax, best_n_states=20, color=None,
                                xlabel=None, ylabel=None, xlim=None, ylim=None, yscale='lin', scale=None,
                                xmajor_loc=None, ymajor_loc=None, majoryTicks=None,
                                savefig=None, format='pdf'):
    cmap = sns.color_palette("Spectral", best_n_states)
    # norm = mpl.colors.Normalize(vmin=0, vmax=best_n_states)

    if color is None:
        color = 'red'

    sns.lineplot(range(0, len(actual)), actual, color=color, ax=ax)

    start = -0.5
    for i, state in enumerate(state_sequence):
        if (i > 0 and state != state_sequence[i - 1]) or (i == (len(state_sequence) - 1)):
            if i == (len(state_sequence) - 1):
                stop = i + 0.5
            else:
                stop = (i - 1) + 0.5
            ax.axvspan(start, stop, color=cmap[state], linewidth=0.0001, alpha=0.3)
            start = i - 0.5

    fontsize = 18
    if ylabel:
        ax.set_ylabel(ylabel, fontsize=fontsize)
    if xlabel:
        ax.set_xlabel(xlabel, fontsize=fontsize)

    if xlim:
        ax.set(xlim=xlim)
    else:
        ax.set(xlim=(-0.5, len(actual) - 0.5))

    if ylim:
        ax.set(ylim=ylim)

    if yscale == "log":
        ax.set_yscale("log")

    if scale:
        if yscale == 'log':
            ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: '{:g}'.format(x / scale)))
        else:
            ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: '{:d}'.format(int(x / scale))))

    if xmajor_loc:
        ax.xaxis.set_major_locator(MultipleLocator(xmajor_loc))
    if ymajor_loc:
        ax.yaxis.set_major_locator(MultipleLocator(ymajor_loc))

    if majoryTicks:
        ax.set_yticks(majoryTicks, minor=False)

    label_ticksize = 14
    # ax.set_xticks(list(ax.get_xticks()[1:-1]) + [len(actual) - 1])
    ax.tick_params(axis='both', which='major', labelsize=label_ticksize)

    labels = [item.get_text() for item in ax.get_yticklabels()]

    if len(labels) == 2:
        labels[0] = 'UP'
        labels[1] = 'DW'
        ax.set_yticklabels(labels)

    if savefig:
        plt.tight_layout()
        plt.savefig(savefig, format=format, bbox_inches='tight', pad_inches=0.05)
    else:
        plt.show()

def get_bounds(data,y,hue, off=0.1,adapt=None):

    g_means={}
    g_devs={}
    groups=np.unique(data[hue].values)

    grouped_data = data.groupby(hue)
    for g in groups:
        data_group=grouped_data.get_group(g)
        g_means[g]=data_group[y].mean()
        g_devs[g]=data_group[y].std()

    ymax=np.max(np.asarray(list(g_means.values()))+np.asarray(list(g_devs.values())))
    ymin=np.min(np.asarray(list(g_means.values()))-np.asarray(list(g_devs.values())))

    if adapt:
        rb=adapt*(round(ymax/adapt)+1) if ymax>adapt else adapt
        lb=adapt*(round(ymin/adapt)-1) if ymin>adapt else 0

        #print(ymax,rb)
        #print(ymin,lb)
    else:
        rb=ymax+ymax*off
        lb=ymin-ymin*off
    return lb,rb


def get_major_minor(data,y,hue,f):
    g_means={}
    g_devs={}
    groups=np.unique(data[hue].values)

    decimal=False
    grouped_data = data.groupby(hue)
    for g in groups:
        data_group=grouped_data.get_group(g)
        g_means[g]=data_group[y].mean()
        g_devs[g]=data_group[y].std()

    ymax=np.max(np.asarray(list(g_means.values()))+np.asarray(list(g_devs.values())))
    ymin=np.min(np.asarray(list(g_means.values()))-np.asarray(list(g_devs.values())))
    print(ymin)
    if f=='PL':
        yM=100
        ym=25
    elif f=='MS':
        print(ymax)
        if ymax<=1e3:
            yM=1e2
            ym=0.25e2
            decimal=True
        elif ymax<=1e4:
            #yM=1e3
            #ym=0.5e3
            yM=2e3
            ym=0.5e3
        elif ymax<=1e5:
            yM=1e4
            ym=5e3
        else:
            yM=1e5
            ym=5e4
            print('Y-ticks',ymax,yM,ym)
    elif f=='IAT':
        '''
        if ymin<1e5:
            yM=1e4
            ym=2500
        elif ymin>1e5:
                yM=1e5
                ym=1e4
        '''
        if ymax<1e3:
            yM=1e2
            ym=0.25e2
            decimal=True
        elif ymax<1e4:
            yM=1e3
            ym=5e2
        elif ymax<1e5:
                yM=1e4
                ym=2.5e3
        else:
            yM=1e5
            ym=2.5e4
    else:
        '''
        if ymin<1e6:
            yM=1e5
            ym=0.25e5
            decimal=True
        elif ymin<1e8:
            yM=1e7
            ym=2500000
        elif ymin>1e8:
                yM=1e8
                ym=1e7
        '''
        if ymax<1e6:
            yM=1e5
            ym=0.25e5
            decimal=True
        elif ymax<1e8:
            yM=1e7
            ym=2500000
        elif ymax>1e8:
                yM=1e8
                ym=1e7
    return yM,ym,decimal



def set_legend(ax,ncol=1,frame_alpha=0.7,loc='lower left',bbox_to_anchor=(0., 1.02, 1., .102),fontsize=10, frameon=False,mode='expand', handles=None ,borderaxespad=0.,labelspacing=0.5, columnspacing=2.0):
    if handles:
        ax.legend(ncol=ncol, framealpha=frame_alpha, bbox_to_anchor=bbox_to_anchor, loc=loc, mode="expand", borderaxespad= borderaxespad,fontsize=fontsize,frameon=frameon,handles=handles,labelspacing=labelspacing,columnspacing=columnspacing)
    else:
        ax.legend(ncol=ncol, framealpha=frame_alpha, bbox_to_anchor=bbox_to_anchor, loc=loc, mode="expand", borderaxespad= borderaxespad,fontsize=fontsize,frameon=frameon,labelspacing=labelspacing,columnspacing=columnspacing)
    
    
def get_10_bound(values):
    ex=np.arange(-40,40,dtype=float)
    dec=np.array([10**e for e in ex])
    l10=dec[np.where(dec<np.min(values))][-1] if np.min(values)>0 else 0
    r10=dec[np.where(dec>np.max(values))][0]
    
    return (l10,r10)
    
    
def plot_grouped_bar_chart(ax, labels_count, start_ind=0,
                           xlim=None, ylim=None, xtick_step=None,
                           color=None, coloredhatch=False, label=None, hatch=None, fontsize=20,
                           xlabel=None, ylabels=None, ylabelscolor='black',
                           update=False, invert_axis=False, scale='linear',
                           label_patches=None, color_patches=None, hatch_patches=None,
                           savefig=None, formatfig='pdf'):
    if color is None:
        color = cmap.colors

    if xlim:
        ax.set_xlim(xlim)
        if scale == 'linear':
            ax.set_xticks(np.arange(xlim[0], xlim[1], xtick_step))
    if ylim:
        ax.set_ylim(ylim)

    # set width of bar
    width = 0.75

    # Set position of bar on X axis
    base_ind = np.arange(len(labels_count))
    ypos = base_ind + start_ind
    num_biflows = list(labels_count)

    if coloredhatch:
        # draw hatch
        ax.barh(ypos, num_biflows, width, color='none', edgecolor=color, hatch=hatch, label=label, zorder=0)
        # draw edge
        ax.barh(ypos, num_biflows, width, color='none', edgecolor='black', zorder=1)
    else:
        ax.barh(ypos, num_biflows, width, color=color, edgecolor='black', label=label, hatch=hatch)

    fontsize = fontsize
    labelsize = fontsize - 2

    ax.set_xlabel(xlabel, fontsize=fontsize)

    if update:
        yticks = list(ax.get_yticks()) + list(ypos)
        yticklabels = [item.get_text() for item in ax.get_yticklabels()] + ylabels
        ax.set_yticks(yticks)
        ax.set_yticklabels(yticklabels)
    else:
        yticks = list(ypos)
        yticklabels = ylabels
        ax.set_yticks(yticks)
        ax.set_yticklabels(yticklabels)

    # where some data has already been plotted to ax
    handles, labels = ax.get_legend_handles_labels()

    if label_patches is not None:
        if color_patches is None:
            color_patches = ['grey'] * len(label_patches)
        if hatch_patches is None:
            hatch_patches = [None] * len(label_patches)

        # manually define a new patch
        for i, label_patch in enumerate(label_patches):
            patch = mpatches.Patch(facecolor=color_patches[i], edgecolor='black',
                                   label=label_patch, hatch=hatch_patches[i])

            # handles is a list, so append manual patch
            handles.append(patch)

    if label is not None:
        # plot the legend
        ax.legend(loc='center', handles=handles,
                  handlelength=2.0, handletextpad=0.75, columnspacing=1.5, fontsize=labelsize,
                  numpoints=1, ncol=3, bbox_to_anchor=(0.5, 1.025), frameon=False)

    if scale == 'symlog':
        ax.set_xscale(scale, subs=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    else:
        ax.set_xscale(scale)
        ax.xaxis.set_minor_locator(MultipleLocator(xtick_step / 5))

    ax.tick_params(axis='x', which='major', right=False, direction='out', labelsize=labelsize)
    ax.tick_params(axis='x', which='minor', right=False, direction='out')

    ax.tick_params(axis='y', which='major', length=0, labelsize=labelsize, colors=ylabelscolor)

    ax.set_axisbelow(True)
    ax.xaxis.grid(True, linestyle='--', linewidth=1, alpha=0.5)
    ax.yaxis.grid(False)

    if invert_axis:
        ax.invert_yaxis()  # labels read top-to-bottom

    majorFormatter = FormatStrFormatter('%d')
    ax.xaxis.set_major_formatter(majorFormatter)

    if savefig:
        plt.tight_layout()
        plt.savefig(savefig, format=formatfig, bbox_inches='tight', pad_inches=0.01)
    else:
        plt.show()
        
        
        
        
def df_line_plot(ax, x, y, data=None,hue=None, hue_order=None,  
                xmajor_loc=None, xminor_loc=None,
                ymajor_loc=None, yminor_loc=None,
                xlabel=None, ylabel=None, xlim=None, ylim=None,
                scale=None, palette=None,color=None,
                savepath=None, format='pdf', 
                decimal=False, annotation=None,linewidth = 1):

    if data:
        bar=sns.lineplot(x=x, y=y, hue=hue,hue_order=hue_order, data=data,ax=ax, palette=palette,linewidth = 1)
    else:
        bar=sns.lineplot(x=x, y=y,ax=ax,color=color,linewidth = 1)
    ax.grid(b=True,linestyle='--', linewidth=0.5, alpha=0.5, zorder=1, axis='y')
    
    if xlabel:
        ax.set_xlabel(xlabel, fontsize=20)
    if ylabel:
        ax.set_ylabel(ylabel, fontsize=20)
    dec=1 if decimal else 0
    if scale:
        if decimal:
            ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: '{:.1f}'.format(x/scale)))
        else:
            ax.yaxis.set_major_formatter(FuncFormatter(lambda x, pos: '{:.0f}'.format(x/scale)))

    if annotation:
        for i,p in enumerate(bar.patches):
            bar.annotate(annotation[i], 
                        (p.get_x() + p.get_width() / 2., p.get_height()), 
                        ha = 'center', va = 'center', 
                        size=11,
                        xytext = (0, 5), 
                        textcoords = 'offset points')

    
    ax.tick_params(axis='both', labelsize=18)

    if xmajor_loc:
        ax.xaxis.set_major_locator(MultipleLocator(xmajor_loc))
    
    if ymajor_loc:
        ax.yaxis.set_major_locator(MultipleLocator(ymajor_loc))

    if xminor_loc:
        ax.xaxis.set_minor_locator(MultipleLocator(xminor_loc))
    if yminor_loc:
        ax.yaxis.set_minor_locator(MultipleLocator(yminor_loc))
    
    if xlim:
        ax.set(xlim=xlim)
    if ylim:
        ax.set(ylim=ylim)

    if savepath:
        plt.tight_layout()
        plt.savefig(savepath, format=format, bbox_inches='tight', pad_inches=0.05)
        
        
        

def plot_boxplot(ax, data, x, y, hue=None,
                 xlim=None, ylim=None, xtick_step=None,
                 colors=None, xlabel=None, ylabel=None,
                 scale='linear', savefig=None, formatfig='pdf',palette=None):
    # Create an array with the colors you want to use
    if colors is None:
        colors = ["#FF0B04", "#4374B3"]

        # Set your custom color palette
        sns.set_palette(sns.color_palette(colors))

    if xlim:
        ax.set_xlim(xlim)
        if scale == 'linear':
            ax.set_xticks(np.arange(xlim[0], xlim[1], xtick_step))
    if ylim:
        ax.set_ylim(ylim)

    sns.boxplot(x=x, y=y, hue=hue, data=data, ax=ax,
                whis=[1, 99], width=0.75, linewidth=1.0, dodge=False,
                showfliers=False,
                flierprops={'marker': 'o', 'markersize': 5, 'markeredgecolor': 'gray',
                            'markeredgewidth': 0.75, 'fillstyle': 'none'},
                showmeans=True,
                meanprops={'marker': 'D', 'markersize': 5, 'markerfacecolor': 'black', 'markeredgecolor': 'none',
                           'markeredgewidth': 0.75, 'fillstyle': 'full'})

    fontsize = 18
    labelsize = 16

    ax.set_xlabel(xlabel, fontsize=fontsize)
    ax.set_ylabel(ylabel, fontsize=fontsize)

    ax.legend(loc='center', handlelength=2.0, handletextpad=0.75, columnspacing=1.5,
              fontsize=labelsize, numpoints=1, ncol=3, bbox_to_anchor=(0.5, 1.025), frameon=False)

    if scale == 'linear':
        ax.set_xscale(scale)
        ax.xaxis.set_minor_locator(MultipleLocator(xtick_step / 5))
    else:
        ax.set_xscale(scale, subs=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

    ax.tick_params(axis='x', which='major', right=False, direction='out', labelsize=labelsize)
    ax.tick_params(axis='x', which='minor', right=False, direction='out')

    ax.tick_params(axis='y', which='major', length=0, labelsize=labelsize)

    ax.set_axisbelow(True)
    ax.xaxis.grid(True, linestyle='--', linewidth=1, alpha=0.5)
    ax.yaxis.grid(False)

    majorFormatter = FormatStrFormatter('%d')
    ax.xaxis.set_major_formatter(majorFormatter)

    if savefig:
        plt.savefig(savefig, format=formatfig, bbox_inches='tight', pad_inches=0.01)
    else:
        plt.show()
        
        
met_unit={
    'bytes_rate_slot':'Bytes/s', 
    'packets_rate_slot':'Packets/s',
    'bits_rate_slot':'Bit/s'
}

direction_dict={
    'UF':'Upstream',
    'DF':'Downstream'
}


dict_palette=['Reds_r','Blues_r','Greens_r','Purples_r','Oranges_r','Greys_r']
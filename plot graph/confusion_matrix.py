from sklearn.metrics import confusion_matrix
import matplotlib.cm as cm
from matplotlib.colors import LogNorm
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

soft_values = np.load('./input_data/dataset_cnn_784_ttl_obfuscated_sf.npy') #read soft values here
prediction_test = soft_values.argmax(axis=-1)
categorical_labels_test = np.load('./input_data/dataset_cnn_784_ttl_obfuscated_clt.npy') #read categorical labels test here

mat = confusion_matrix(categorical_labels_test, prediction_test, normalize = 'true')

#Seaborn CM plot
plt.figure(figsize=(12, 8), dpi=70)
cmap = cm.get_cmap('PuBuGn')
sns.heatmap(mat.T*100, square=True, annot=False,
    vmin=0.0, vmax=100.0, cmap=cmap, cbar=True
)
sns.set(font_scale=1.4)
plt.xlabel('True Label')
plt.ylabel('Predicted Label')
plt.title('Confusion Matrix of DL Traffic Classifier')

#Seaborn CM plot in log scale
plt.figure(figsize=(12, 8), dpi=70)
sns.heatmap(mat.T*100, square=True, annot=False,
    vmin=0.0, vmax=100.0, cmap=cmap,
    cbar=True, norm=LogNorm(vmin=0.01, vmax=100.0)
)
sns.set(font_scale=1.4)
plt.xlabel('True Label')
plt.ylabel('Predicted Label')
plt.title('Confusion Matrix of DL Traffic Classifier in Log-scale')
plt.show()

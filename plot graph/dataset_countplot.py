import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter, MultipleLocator
import seaborn as sns
import numpy as np
from glob import glob
import shelve

""" label_count_dict = {
    '-': 76701, 
    'C&C': 21995, 
    'C&C-FileDownload': 53, 
    'C&C-HeartBeat': 33673, 
    'C&C-HeartBeat-Attack': 834, 
    'C&C-HeartBeat-FileDownload': 11, 
    'C&C-Torii': 30, 
    'DDoS': 39897, 
    'FileDownload': 18, 
    'Okiru': 152474, 
    'PartOfAHorizontalPortScan': 534626, 
    'C&C-PartOfAHorizontalPortScan': 888, 
    'Attack': 9398
} """

outdir = './input_data/'

df = pd.read_csv(f"{outdir}/label_count.csv")

# sns.set(font_scale = 1.3)
# sns.set_style("whitegrid", {'grid.linestyle': '--'})

fig, ax = plt.subplots(figsize=(8.4, 3.2))

color = '#FF5F1F'
hatch =  '/' * 2
ax = sns.countplot(y="label", data=df, color=color, order = df['label'].value_counts().index, ax=ax,
                   edgecolor='black', hatch=hatch)

ax.set_xscale('log')

fontsize = 14

ax.set_xlabel("Number of Biflows", fontsize=fontsize)
ax.set_ylabel(None)

ax.set_axisbelow(True)
ax.xaxis.grid(True, linestyle='--', linewidth=1, alpha=0.5)
ax.yaxis.grid(False)


ax.tick_params(axis='x', which='major', right=False, direction='out', labelsize=fontsize)
ax.tick_params(axis='x', which='minor', right=False, direction='out')

ax.tick_params(axis='y', which='major', length=0, rotation=0, labelsize=fontsize)

ax.xaxis.set_major_formatter(FormatStrFormatter("%g"))

# plt.show()
plt.tight_layout()
plt.savefig(f'{outdir}/dataset_sampled.pdf', format='pdf', bbox_inches='tight', pad_inches=0.01)
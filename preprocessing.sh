#!/bin/bash
cd rsc/$1/
#mkdir pcap
#tcpdump -r *.pcap -w pcap/capture -C 50

cd ../
cd ../
python3 biflows_processing.py rsc/$1/

cd csv\ gen
python3 csv_for_sampling.py rsc/$1/biflows.dat

cd ../
python3 sampling.py rsc/$1/

# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import shelve
import traceback
import sys
import os
from decimal import Decimal
from datetime import datetime

sys.path.insert(1, './src/util')

import dir_manager as mng
# %%

if __name__ == "__main__":

    if len(sys.argv) == 2:

        print("File name: " + sys.argv[1]) 
        file_name = sys.argv[1]
        CAPTURE = file_name.split('/')[1]
        mng.make_dir('./rsc/dat_sampled')

        biflows_csv = open(file_name + 'biflows_sampled.csv', 'r')
        biflows = shelve.open(file_name + 'biflows.dat')
        biflows_sampled = shelve.open('./rsc/dat_sampled/' + CAPTURE + '_sampled.dat', writeback=True)

        file_size = os.path.getsize(file_name+'biflows.dat')

        start = datetime.now()
        print('sampling ' + CAPTURE + '.dat')

        next(biflows_csv)

        try:
            for i,line in enumerate(biflows_csv):
                splitted_line = line.split(';')
                quintuple = splitted_line[0]
                ts = Decimal(splitted_line[1])

                if file_size > 5368709120 and i%5000000 == 0:
                    print('flushing biflows sampled')
                    biflows_sampled.sync()
                    
                for flow in biflows[quintuple]:
                    if flow['ts'] == ts:
                        if quintuple not in biflows_sampled:
                            biflows_sampled[quintuple] = []
                            biflows_sampled[quintuple].append(flow)
                        elif quintuple in biflows_sampled:
                            biflows_sampled[quintuple].append(flow)
        except:
            biflows.close()
            biflows_sampled.close()
            biflows_csv.close()
            print(traceback.format_exc())

        biflows.close()
        biflows_sampled.close()
        biflows_csv.close()

        mng.delete_dir(file_name + 'biflows_sampled.csv', False)

        end = datetime.now()
        t = end - start
        print('sampling done in : ' + str(t))

    else:
        print("Insert two args")

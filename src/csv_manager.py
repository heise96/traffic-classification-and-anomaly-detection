import csv 
from random import sample
from datetime import datetime

csv_columns = ['quintuple','ts','detailed_label']

to_delete_classes = ['C&C-Mirai', 'PartOfAHorizontalPortScan-Attack', 'Okiru-Attack']
to_sample_classes = ['-', 'DDoS', 'Okiru', 'PartOfAHorizontalPortScan']


def fill_class_count(csv_file, class_count):

    print("counting classes in csv")
    start = datetime.now()

    with open("../"+csv_file, 'r') as in_file:

        reader = csv.reader(in_file, delimiter=';')
        next(reader)

        for i,line in enumerate(reader):

            class_name = line[-1] #class_name = line[-2].lower()
            class_count[class_name].append(i+1)

    end = datetime.now()-start
    print("counting ended in " + str(end))


def sampling(csv_file, class_count):

    print("sampling csv")
    start = datetime.now()
    PATH = csv_file.split("/")
    PATH = "../"+PATH[0] + "/" + PATH[1]
    file_name = csv_file.split('/')[-1]

    with open(PATH+"/"+file_name[:len(file_name)-4] +'_sampled.csv', 'w') as out_file:

        out_file.writelines(';'.join(csv_columns) + '\n')

        with open("../"+csv_file, 'r') as in_file:

            random_line_seq = []

            for class_name, count in class_count.items():

                if class_name in to_delete_classes:
                    continue
                elif class_name in to_sample_classes:
                    k = int(0.0025*(len(count)))
                
                    if k==0:
                        continue
                    
                    sampling_range = count
                    temp = sample(sampling_range, k)
                    random_line_seq.extend(temp)
                else:
                    random_line_seq.extend(count)

            if len(random_line_seq)==0:
                return

            random_line_seq.sort(reverse=True)
            current_line = random_line_seq.pop()
            
            for n, line in enumerate(in_file):
                if n == current_line: 

                    out_file.writelines(line)
                    if len(random_line_seq) > 0:
                        current_line = random_line_seq.pop()
                    else:
                        break

    end = datetime.now()-start
    print("csv sampled in " + str(end))
import sys
import traceback
import shelve
from decimal import Decimal


def log_split(log_path):

    max_dim = 3000000

    path = log_path.split('/')
    path = path[0]+"/"+path[1]

    with open(log_path, "r") as in_file:
        line_count = sum(1 for line in in_file)
        in_file.seek(0)
        if line_count < max_dim:
            print('no split needed')
            
            with open(path+'/splitted_log/log0', 'w') as out_file:
                for i,line in enumerate(in_file):
                    if i>7 and i!= line_count-1:
                        out_file.writelines(line)

        else:
            j = 0
            lines = []
            print('line to split -> ' + str(line_count))

            for i,line in enumerate(in_file):

                if i>7 and i != line_count-1:
                    if i%max_dim == 0:
                        with open(path+'/splitted_log/log' + str(j), 'w') as out_file:
                            out_file.writelines(lines)
                        lines = []
                        j = j+1
                        print('splitting next portion - now doing: ' + str(j))


                    lines.append(line)

            if len(lines) > 0: 
                with open(path+'/splitted_log/log' + str(j), 'w') as out_file:
                    out_file.writelines(lines)



def get_biflows(log_path, log_biflows):    
    #leggo il file biflows
    biflows = shelve.open(log_biflows, writeback=True)

    with open(log_path, "r") as log_file:
        try:
            for line in log_file:

                line = line.strip().replace('\x20\x20\x20', '\x09').replace('\x09',',') #modifico i caratteri separatori
                splitted_line = line.split(',') #split di ogni linea sul carattere ','

                #definisco i campi di interesse
                timestamp = splitted_line[0]
                src_addr = splitted_line[2]
                src_port = splitted_line[3]
                dst_addr = splitted_line[4]
                dst_port = splitted_line[5]
                label = splitted_line[21]
                detailed_label = splitted_line[22]
                
                #codifico il campo proto con un valore numerico
                if splitted_line[6] == 'tcp':
                    proto = '6'
                elif splitted_line[6] == 'udp':
                    proto = '17'
                else:
                    proto = 'nd'

                #definisco quintupla e quintupla inversa
                quintuple = '%s,%s,%s,%s,%s' % (src_addr,src_port,dst_addr,dst_port,proto)

                #definisco l'elemento value
                value = {
                    'ts' : Decimal(timestamp),
                    'label' : label,
                    'detailed-label' : detailed_label
                }


                if quintuple not in biflows:
                    biflows[quintuple] = [] #definisco l'array
                    biflows[quintuple].append(value) #appendo il valore
                elif quintuple in biflows:
                    biflows[quintuple].append(value) #appendo il valore
                else:
                    sys.stderr.write('error')
            biflows.close()
        except:
            print(traceback.format_exc())
            biflows.close()
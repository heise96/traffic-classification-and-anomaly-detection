import numpy as np
from glob import glob
import shelve
import re


def tryint(s):
    try:
        return int(s)
    except:
        return s

def alphanum_key(s):
    return [ tryint(c) for c in re.split('([0-9]+)', s) ]

def sort_nicely(l):
    l.sort(key=alphanum_key)


def calculate_iat(timestamp_array):

    iat_array = []
    cons_pkt = []

    for ts in timestamp_array:

        cons_pkt.append(ts)

        if len(cons_pkt) < 2:
            interarrival_time = 0
        else:
            interarrival_time = np.diff(cons_pkt)[0]
            cons_pkt = cons_pkt[1:]
        if interarrival_time < 0:
            raise ValueError('ERROR -> IAT Negative ')
        iat_array.append(float(interarrival_time))

    return iat_array


def merge_dict_on_log(pcap_biflows, log_biflows, merged_biflows):

    sc = 0

    for quintuple, value_list in log_biflows.items():
        index = 0
    
        #Uncomment for large file
        sc += 1
        if sc%1000000==0:
            print('flushing')
            merged_biflows.sync()
        
        
        for i,pck in enumerate(value_list):

            if(len(value_list) != i+1):
                timestamp = value_list[i+1]['ts']
            else:
                timestamp = pck['ts'] + 1  

            packets = [] 
            temp = quintuple.split(',')
            inverse_quintuple = temp[2]+','+temp[3]+','+temp[0]+','+temp[1]+','+temp[4]
            
            if quintuple in pcap_biflows:
                packets.extend(pcap_biflows[quintuple])
            if inverse_quintuple in pcap_biflows:
                packets.extend(pcap_biflows[inverse_quintuple])

            if len(packets) != 0:
                packets.sort(key=lambda x : x['ts'])
                count=0

                biflows_labeled = {
                    'ts' : 0,
                    'pl' : [],
                    'pd' : [],
                    'ip' : [],
                    'transport' : [],
                    'payl' : [],
                    'direction' : [],
                    'ws' : [],
                    'ttl' : [],
                    'flags' : [],
                    'iat' : []
                }

                timestamps = []
                while index < len(packets) and packets[index]['ts'] != timestamp:
                    biflows_label = pck['label']
                    biflows_det_label = pck['detailed-label']
                    timestamps.append(packets[index]['ts'])

                    biflows_labeled['pl'].append(packets[index]['pl'])
                    biflows_labeled['pd'].append(packets[index]['pd'])
                    biflows_labeled['ip'].append(packets[index]['ip'])
                    biflows_labeled['transport'].append(packets[index]['transport'])
                    biflows_labeled['payl'].append(packets[index]['payl'])
                    biflows_labeled['direction'].append(packets[index]['direction'])
                    biflows_labeled['ws'].append(packets[index]['ws'])
                    biflows_labeled['ttl'].append(packets[index]['ttl'])
                    biflows_labeled['flags'].append(packets[index]['flags'])

                    index = index+1
                    count = count+1

                if timestamps == []:
                    print("skipping ---> "+ quintuple)
                    continue
                    #raise ValueError('ERROR -> ' + str(quintuple))

                biflows_labeled['ts'] = timestamps[0]
                biflows_labeled['label'] = biflows_label
                biflows_labeled['detailed-label'] = biflows_det_label
                biflows_labeled['iat'] = calculate_iat(timestamps)

                if quintuple not in merged_biflows:
                    merged_biflows[quintuple] = []
                    merged_biflows[quintuple].append(biflows_labeled)
                else:
                    merged_biflows[quintuple].append(biflows_labeled)

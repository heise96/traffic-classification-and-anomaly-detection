import sys
from scapy.all import PcapReader, IP, TCP, UDP, DNS, NTP, Raw


def get_biflows(packet, biflows):
      if packet.haslayer(IP) and (packet.haslayer(TCP) or packet.haslayer(UDP)):
        if not (packet.haslayer(NTP) or packet.haslayer(DNS)):

            layer_src_ip = packet[IP].src
            layer_dst_ip = packet[IP].dst

            layer_src_port = packet.sport
            layer_dst_port = packet.dport

            src_socket = '%s,%s' % (layer_src_ip, layer_src_port)
            dst_socket = '%s,%s' % (layer_dst_ip, layer_dst_port)
            proto = packet[IP].proto

            quintuple = '%s,%s,%s' % (src_socket, dst_socket, proto)
            inverse_quintuple = '%s,%s,%s' % (dst_socket, src_socket, proto)

            timestamp = packet.time
            packet_length = packet[IP].len
            ttl = packet[IP].ttl

            #IP header
            ip_pkt = packet[IP].copy()
            ip_pkt.remove_payload()
       
            if packet.haslayer(TCP):
                window_size = packet[TCP].window
                tcp_flags = packet[TCP].flags
                
                #TCP header
                tcp_pkt = packet[TCP].copy()
                tcp_pkt.remove_payload()
                transport_header = tcp_pkt
            else:
                window_size = 0
                tcp_flags = [0]*6

                #UDP header
                udp_pkt = packet[UDP].copy()
                udp_pkt.remove_payload()
                transport_header = udp_pkt

            if packet.haslayer(Raw):
                payload_data = packet.getlayer(Raw).load[:2048]
                payload_length = len(packet.getlayer(Raw).load)
            else:
                payload_data = ''
                payload_length = 0

            lwg_pck = {
                'ts' : timestamp,
                'pl' : packet_length,
                'pd' : payload_data,
                'payl' : payload_length,
                'ws' : window_size,
                'ttl' : ttl,
                'flags' : tcp_flags,
                'ip' : bytes(ip_pkt),
                'transport' : bytes(transport_header)
            }

            if quintuple not in biflows and inverse_quintuple not in biflows:
                lwg_pck['direction'] = 1
                biflows[quintuple] = []
                biflows[quintuple].append(lwg_pck)
            elif quintuple in biflows:
                lwg_pck['direction'] = 1
                biflows[quintuple].append(lwg_pck)
            elif inverse_quintuple in biflows:
                lwg_pck['direction'] = -1
                biflows[inverse_quintuple].append(lwg_pck)
            else:
                sys.stderr.write('No bifl for %s -> %s' % (src_socket, dst_socket))
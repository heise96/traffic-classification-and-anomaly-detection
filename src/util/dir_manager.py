import os
import shutil
import bz2
import traceback
import re


def make_dir(path):
    try:
        os.mkdir(path)
    except OSError:
        print ("Creation of the directory %s failed" % path)
    else:
        print ("Successfully created the directory %s" % path)


def delete_dir(path, is_dir):
    try:
        if is_dir:
            shutil.rmtree(path)
        else:
            os.remove(path)
    except:
        print(traceback.format_exc())
        print("Delete of  %s failed" % path)
    else:
        print ("Successfully deleted  %s " % path)


def compress_file(file_to_compress):

    file_compressed = file_to_compress + '.bz2'

    with open(file_to_compress, 'rb') as input:
        with bz2.BZ2File(file_compressed, 'wb', compresslevel=9) as output:
            shutil.copyfileobj(input, output)

    print('file compressed')


def tryint(s):
    try:
        return int(s)
    except:
        return s

def alphanum_key(s):
    return [ tryint(c) for c in re.split('([0-9]+)', s) ]

def sort_nicely(l):
    l.sort(key=alphanum_key)